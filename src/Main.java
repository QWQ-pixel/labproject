
public class Main{
    public static void main(String[] args){
       int [] num1 = new int[10];
       int [] num2 = new int[10];

       for(int i = 0; i < 10; i++){
            num1[i] = ((int)(Math.random() * 100) - 1);
            num2[i] = ((int)(Math.random() * 100) - 1);
       }

       System.out.println("Первый массив");
       print(num1);

       System.out.println("Второй массив");
       print(num2);

       System.out.printf("Начинаем записывать с %s позиции\n", args[0]);
       num1 = changeArray(num1, num2, args[0]);

       System.out.println("Обновленный массив");
       print(num1);

    }

    public static int[] changeArray(int[] arr1, int[] arr2, String diaposonStarts){
        int start = Integer.parseInt(diaposonStarts);

        for (int i = start; i < arr1.length; i++) {
                arr1[i] = arr2[i];
        }
        return arr1;
    }

    public static void print(int[] array){
        for (int i : array) {
          System.out.printf("%d ",i);
        }
        System.out.println("\n");
    }

}
